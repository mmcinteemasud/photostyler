from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
import cv2
import numpy as np
import os
from .forms import UploadForm
from .utils import Style
from .models import Photo
# Create your views here.

@login_required
def Uploader(request):
	if request.method == 'POST':
		user = request.user
		form = UploadForm(request.POST, request.FILES)
		if form.is_valid():
			name = request.FILES['image'].name
			filename, file_extension = os.path.splitext(name)
			photo = cv2.imdecode(np.fromstring(request.FILES['image'].read(), np.uint8), cv2.IMREAD_UNCHANGED)
			style = form.cleaned_data['style']
			Style(photo, style, user, filename)
			return redirect('gallery')
	else:
		form = UploadForm()
	return render(request, 'styler/uploader.html', {'form':form})

@login_required
def Gallery(request):
	user = request.user
	photos = Photo.objects.filter(user=user)
	return render(request, 'styler/gallery.html', {'photos':photos})

@login_required
def Delete(request, pk):
	user=request.user
	photo=Photo.objects.get(pk=pk, user=user)
	photo.delete()
	return redirect('gallery')

@login_required
def Detail(request, pk):
	user=request.user
	photo=Photo.objects.get(pk=pk, user=user)
	return render(request, 'styler/detail.html', {'photo':photo})


