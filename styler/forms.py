from django import forms
from .models import Photo


class UploadForm(forms.ModelForm):
	CHOICES=[('pencil', 'Pencil'),('charcoal', 'Charcoal'), ('edges', 'Edges'), ('cartoon', 'Cartoon'),]
	style = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect, required=True)
	class Meta:
		model = Photo
		fields = ('image', 'style')