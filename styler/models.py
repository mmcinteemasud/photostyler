from django.db import models
from django.conf import settings


# Create your models here.
class Photo(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            related_name='photos', on_delete=models.CASCADE)
	image = models.FileField(upload_to='media/%Y/%m')
	saved_on = models.DateTimeField(auto_now_add=True)
	class Meta:
    		ordering = ["-saved_on"]

	def __str__(self):
 		return str(self.image)

