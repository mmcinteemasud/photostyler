import cv2
import numpy as np
from .models import Photo
from django.core.files import File
import os

def Style(photo, style, user, filename):
	if style=='pencil':
		name = filename + "_pencil.jpg"
		img_gray = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
		img_blur = cv2.GaussianBlur(img_gray, (21,21), 0, 0)
		image = cv2.divide(img_gray, img_blur, scale=256)
		cv2.imwrite(name, image)
		with open(name, 'rb') as file:
			Photo.objects.create(user=user, image=File(file))
		os.remove(name)

	if style=='charcoal':
		name = filename + "_charcoal.jpg"
		img = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
		img = cv2.Laplacian(img,cv2.CV_64F)
		image = cv2.addWeighted( img, 0, img, 7, 10)
		cv2.imwrite(name, image)
		with open(name, 'rb') as file:
			Photo.objects.create(user=user, image=File(file))
		os.remove(name)

	if style=='edges':
		name = filename + "_edges.jpg"
		img_small = cv2.pyrDown(photo)

		num_iter = 25
		for _ in range(num_iter):
			img_small = cv2.bilateralFilter(img_small, d=9, sigmaColor=9,sigmaSpace=7)

		img_rgb = cv2.pyrUp(img_small)
		img_color = img_rgb
		img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
		img_blur = cv2.medianBlur(img_gray, 7)

		# -- STEP 4 --
		# detect and enhance edges
		img_edge = cv2.adaptiveThreshold(img_blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 2)
		# -- STEP 5 --
		# convert back to color so that it can be bit-ANDed
		# with color image

		image = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
		cv2.imwrite(name, image)
		with open(name, 'rb') as file:
			Photo.objects.create(user=user, image=File(file))
		os.remove(name)

	if style=='cartoon':
		name = filename + "_cartoon.jpg"
		img_small = cv2.pyrDown(photo)

		num_iter = 25
		for _ in range(num_iter):
			img_small = cv2.bilateralFilter(img_small, d=9, sigmaColor=9,sigmaSpace=7)

		img_rgb = cv2.pyrUp(img_small)
		img_color = img_rgb
		img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
		img_blur = cv2.medianBlur(img_gray, 7)

		# -- STEP 4 --
		# detect and enhance edges
		img_edge = cv2.adaptiveThreshold(img_blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 2)
		# -- STEP 5 --
		# convert back to color so that it can be bit-ANDed
		# with color image

		img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
		image =  cv2.bitwise_and(img_color, img_edge)
		cv2.imwrite(name, image)
		with open(name, 'rb') as file:
			Photo.objects.create(user=user, image=File(file))
		os.remove(name)